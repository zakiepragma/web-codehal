var typed = new Typed(".multiple-text", {
  strings: ["Full Stack Developer", "Laravel Developer", "React Developer"],
  typedSpeed: 100,
  backSpeed: 100,
  backDelay: 1000,
  loop: true,
});
